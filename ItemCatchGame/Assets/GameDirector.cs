﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    GameObject hpGauge;
    GameObject WhiteArrow;
    GameObject Score;

    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge");
        this.WhiteArrow = GameObject.Find("WhiteArrow");
        this.Score = GameObject.Find("Score");
    }

    int score = 0;

    public void IncreaseScore()
    { 
        score += 10;
        this.Score.GetComponent<Text>().text = "SCORE:" + score.ToString("D3") + "/300";
        if(score == 300)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void DecreaseHp()
    {
        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;
    }
}
