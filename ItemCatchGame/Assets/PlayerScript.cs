﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (this.transform.position.y < 3)
            {
                transform.Translate(0, 0.1f, 0);
            }
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (this.transform.position.y > -3)
            {
                transform.Translate(0, -0.1f, 0);
            }
        }
    }
}
